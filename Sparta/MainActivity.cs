﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using System.Linq;
using Android.Animation;
using Android.Content.PM;
using Android.Graphics;

namespace Sparta
{
	[Activity(
		Label = "Sparta",
		MainLauncher = true,
		Icon = "@drawable/icon",
		Theme = "@style/MyTheme",
		ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : Activity, AbsListView.IOnScrollListener, Animator.IAnimatorListener
	{
		View headerView;
		CustomListView listView;
		float scale;
		IList<Item> listItems = new List<Item>();

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.Main);
			scale = Resources.DisplayMetrics.Density;

			// header padding
			var placeholder = new View(this);
			placeholder.SetMinimumWidth ((int) (170 * scale + 0.5f));
			placeholder.SetMinimumHeight ((int) (170 * scale + 0.5f));

			LoadData ();

			listView = FindViewById<CustomListView> (Resource.Id.list);
			listView.AddHeaderView (placeholder);
			listView.Adapter = new DataAdapter (this, listItems);
			listView.SetOnScrollListener (this);
			listView.ItemFling += HandleItemFling;

			headerView = FindViewById (Resource.Id.layout_header);

		}

		void LoadData() {
			this.listItems = Resources.GetStringArray(Resource.Array.data_source).Select((s, i) => {

				int n = i + 1;
				var str = s.Split(new []{" | "}, StringSplitOptions.None);
				int image;

				if (n % 5 == 0) image = Resource.Drawable.v;
				else if (n % 4 == 0) image = Resource.Drawable.iv;
				else if (n % 3 == 0) image = Resource.Drawable.iii;
				else if (n % 2 == 0) image = Resource.Drawable.ii;
				else image = Resource.Drawable.i;

				return new Item
				{
					Title = str[0],
					Source = str[1],
					Date = str[2],
					Image = image
				};
			}).ToList();
		}

		void HandleItemFling(int index, CustomListView.FlingDirection direction)
		{
			//Toast.MakeText (this, index.ToString(), ToastLength.Short).Show();

			if (index == 0)
				return;

			int first = listView.FirstVisiblePosition - listView.HeaderViewsCount;
			int child = index - first - 1;

			if (child < 0 || child >= listView.ChildCount) {
				return;
			}

			View view = listView.GetChildAt(child);

			int i = index - 1;

			bool unfolded = listItems [i].Unfolded;

			if (direction == CustomListView.FlingDirection.Left && !unfolded) {
				listItems [i].Unfolded = true;
				ShowButtons (view);
			}

			if (direction == CustomListView.FlingDirection.Right && unfolded) {
				listItems [i].Unfolded = false;
				HideButtons (view);
			}
		}

		void ShowButtons (View view)
		{
			var infoContainer = view.FindViewById<RelativeLayout> (Resource.Id.container_info);
			var buttonsContainer = view.FindViewById<LinearLayout> (Resource.Id.container_buttons);

			int shift = buttonsContainer.Width;

			var animator1 = ObjectAnimator.OfFloat(infoContainer, "X", infoContainer.GetX(), infoContainer.GetX() - shift);
			var animator2 = ObjectAnimator.OfFloat(buttonsContainer, "X", buttonsContainer.GetX(), buttonsContainer.GetX() - shift);
			animator1.SetDuration(150);
			animator2.SetDuration(150);
			animator1.Start();
			animator2.Start();
		}

		void HideButtons (View view)
		{
			var infoContainer = view.FindViewById<RelativeLayout> (Resource.Id.container_info);
			var buttonsContainer = view.FindViewById<LinearLayout> (Resource.Id.container_buttons);

			int shift = buttonsContainer.Width;

			var animator1 = ObjectAnimator.OfFloat(infoContainer, "X", infoContainer.GetX(), infoContainer.GetX() + shift);
			var animator2 = ObjectAnimator.OfFloat(buttonsContainer, "X", buttonsContainer.GetX(), buttonsContainer.GetX() + shift);
			animator1.SetDuration(150);
			animator2.SetDuration(150);
			animator1.Start();
			animator2.Start();
		}

		bool showAnimation = true;

		void ShowHeaderView()
		{
			if (!showAnimation) {

				headerView.Tag = new Tag { Show = true };

				var animator = ObjectAnimator.OfFloat(headerView, "Y", headerView.GetY(), 0);
				animator.AddListener (this);
				animator.SetDuration(300);
				animator.Start();

				showAnimation = true;
			}
		}

		void HideHeaderView()
		{
			if (showAnimation) {

				headerView.Tag = new Tag { Show = true };

				var animator = ObjectAnimator.OfFloat(headerView, "Y", headerView.GetY(), -(200 * scale + 0.5f));
				animator.AddListener (this);
				animator.SetDuration(300);
				animator.Start();

				showAnimation = false;
			}
		}

		//
		// Scrolling

		int prevVisibleItem = 0;

		void AbsListView.IOnScrollListener.OnScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
		{
			// when scrolling up
			if (firstVisibleItem >= 1 && prevVisibleItem < firstVisibleItem) {
				HideHeaderView ();
			}

			// when scrolling down
			if (prevVisibleItem > firstVisibleItem) {
				ShowHeaderView ();
			}

			//if (firstVisibleItem != prevVisibleItem) {
			//	Toast.MakeText (this, firstVisibleItem.ToString (), ToastLength.Short).Show ();
			//}

			prevVisibleItem = firstVisibleItem;
		}

		void AbsListView.IOnScrollListener.OnScrollStateChanged(AbsListView view, ScrollState scrollState)
		{

		}

		//
		// Animation

		Animator animation = null;

		void Animator.IAnimatorListener.OnAnimationStart (Animator animation)
		{
			if (this.animation != null) {
				this.animation.Cancel ();
			}

			headerView.Visibility = ViewStates.Visible;

			this.animation = animation;
		}

		void Animator.IAnimatorListener.OnAnimationEnd (Animator animation)
		{
			bool show = ((Tag)headerView.Tag).Show;

			if (!show) {
				headerView.Visibility = ViewStates.Invisible;
			}

			this.animation = null;
		}

		void Animator.IAnimatorListener.OnAnimationCancel (Animator animation)
		{
			this.animation = null;
		}

		void Animator.IAnimatorListener.OnAnimationRepeat (Animator animation)
		{

		}

		//
		// Helper classes

		class Item
		{
			public int Image { get; set; }
			public string Title { get; set; }
			public string Source { get; set; }
			public string Date { get; set; }
			public bool Unfolded { get; set; }
		}

		class Tag : Java.Lang.Object
		{
			public bool Show { get; set; }
		}

		class DataAdapter : BaseAdapter<Item>
		{
			MainActivity activity;
			IList<Item> data;

			public DataAdapter (MainActivity activity, IList<Item> data) : base()
			{
				this.activity = activity;
				this.data = data;
			}

			public override long GetItemId(int position)
			{
				return position;
			}

			public override Item this[int position] {  
				get { return data[position]; }
			}

			public override int Count {
				get { return data.Count; }
			}

			public override View GetView (int position, View convertView, ViewGroup parent)
			{
				FrameLayout layout;
				if (convertView != null) {
					layout = (FrameLayout)convertView;
				} else {
					layout = (FrameLayout) activity.LayoutInflater.Inflate (Resource.Layout.Row, parent, false);
				}

				RelativeLayout infoContainer = layout.FindViewById<RelativeLayout> (Resource.Id.container_info);
				LinearLayout buttonsContainer = layout.FindViewById<LinearLayout> (Resource.Id.container_buttons);
				buttonsContainer.Visibility = ViewStates.Visible;

				TextView titleView = layout.FindViewById<TextView> (Resource.Id.textview_title);
				TextView dateView = layout.FindViewById<TextView> (Resource.Id.textview_date);
				TextView sourceView = layout.FindViewById<TextView> (Resource.Id.textview_source);
				ImageView imageView = layout.FindViewById<ImageView> (Resource.Id.image_thumb);

				var item = data [position];

				int shift = buttonsContainer.Width;

				if (item.Unfolded) {
					infoContainer.SetX (parent.Left - shift);
					buttonsContainer.SetX (parent.Right - shift);
				} else {
					infoContainer.SetX (parent.Left);
					buttonsContainer.SetX (parent.Right);
				}

				imageView.SetImageResource (item.Image);
				titleView.Text = item.Title;
				sourceView.Text = item.Source;
				dateView.Text = item.Date;

				return layout;
			}
		}
	}
}

