﻿using System;
using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Sparta
{
	public class CustomListView : ListView, GestureDetector.IOnGestureListener
	{
		public event Action<int, FlingDirection> ItemFling;

		public enum FlingDirection
		{
			Left,
			Right
		}

		GestureDetector gestureDetector;

		public CustomListView(Context context) : base(context)
		{
			gestureDetector = new GestureDetector (context, this);
		}

		public CustomListView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			gestureDetector = new GestureDetector (context, this);
		}

		public CustomListView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
		{
			gestureDetector = new GestureDetector (context, this);
		}

		public override bool OnTouchEvent (MotionEvent e)
		{
			gestureDetector.OnTouchEvent (e);
			return base.OnTouchEvent (e);
		}

		bool GestureDetector.IOnGestureListener.OnFling (MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			float diffY = e2.GetY() - e1.GetY();
			float diffX = e2.GetX() - e1.GetX();

			// if horizontal swipe
			if (Math.Abs (diffX) > Math.Abs (diffY)) {
				// and it has enough velocity
				if (Math.Abs(diffX) > 100 && Math.Abs(velocityX) > 100) {

					int index = PointToPosition ((int)e1.GetX (), (int)e1.GetY ());

					if (diffX > 0) {
						if (ItemFling != null) ItemFling (index, FlingDirection.Right);
					} else {
						if (ItemFling != null) ItemFling (index, FlingDirection.Left);
					}

					return true;
				}
			}

			return false;
		}

		bool GestureDetector.IOnGestureListener.OnDown (MotionEvent e)
		{
			return true;
		}

		void GestureDetector.IOnGestureListener.OnLongPress (MotionEvent e)
		{
		}

		bool GestureDetector.IOnGestureListener.OnScroll (MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			return false;
		}

		void GestureDetector.IOnGestureListener.OnShowPress (MotionEvent e)
		{
		}

		bool GestureDetector.IOnGestureListener.OnSingleTapUp (MotionEvent e)
		{
			return false;
		}
	}
}

